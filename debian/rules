#!/usr/bin/make -f

DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

# start by saving the path to debian/rules (useful in get-orig-source)
PATH_TO_DEBIAN := $(shell dirname $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))

ifneq (,$(filter $(DEB_HOST_ARCH), ia64))
DEB_CPPFLAGS_MAINT_APPEND = -DMM_MIN_ALIGNMENT=16
endif
DPKG_EXPORT_BUILDFLAGS = 1
-include /usr/share/dpkg/buildflags.mk
CFLAGS += $(CPPFLAGS)
export CFLAGS

# Fixes FTBFSes of plug-ins under Hurd
ifneq (,$(filter $(DEB_HOST_ARCH), hurd-i386))
	LDFLAGS += -lpthread
endif
export LDFLAGS

# This has to be exported to make some magic below work.
export DH_OPTIONS

check:
	dh_testdir
ifeq (,$(filter nocheck, $(DEB_BUILD_OPTIONS)))
# Bug 580524: ia64 hurd-i386 mips
# xvfb-run failing too often: arm64 armhf i386 ppc64el powerpc x32
ifeq (,$(filter $(DEB_HOST_ARCH), ia64 hurd-i386 mips))
	xvfb-run relocate/bin/yorick -batch debian/check.i
endif
endif

configure: configure-stamp
configure-stamp:
	dh_testdir
	make Y_HOME=relocate ysite
	chmod a+x debian/helpers/*.i
	touch configure-stamp


BUILDDIR=../../build/
build: build-arch build-indep
build-arch: build-stamp build-mpy-stamp

build-indep: build-indep-stamp
build-indep-stamp: build-stamp
	sed 's/^.*Printed: .*$$//' -i build/doc/*.doc
	cp debian/helpers/README build/doc
	convert icons/yicon32.png build/yorick32x32.xpm
	convert icons/yicon16.png build/yorick16x16.xpm
	$(MAKE) -C doc/html BUILDDIR=$(BUILDDIR) manual refcard $(BUILDDIR)doc/README.html $(BUILDDIR)doc/images/triangle3.jpg $(BUILDDIR)doc/skull.css
	cp doc/html/packinfo.txt build/00yorick.packinfo
	cp doc/html/keywords.txt build/00yorick.keywords
	cp doc/html/aliases.txt build/00yorick.aliases
	find build/doc/manual -name "*.html" -not -name yorick_prt.html | xargs sed -e 's|^<body .*$$|&<div id="l-bar"><div id="links"><div class="nav1"><A href="http://dhmunro.github.io/yorick-doc/">Web Home</A></div><div class="nav1"><a href="../README.html">Getting Started</a></div><div class="nav1"><A href="../manual/index.html">Manual</A></div><div class="nav1"><A href="../html_xref/packages.html">Packages</A></div><div class="nav1"><a href="../refcard/index.html">Quick Reference</a></div></div><div id="bottomnav"></div></div><div id="banner"></div><div id="content">|' -e 's|</body>|</div>&|' -i
	sed 's|yorick_1.html|index.html|' -i build/doc/*.html build/doc/*/*.html
	touch build-indep-stamp

build-stamp: configure-stamp 
	dh_testdir
	$(MAKE) CDG_FLAGS="$(CFLAGS) $(LDFLAGS)" install
	-rm relocate/LICENSE.md
	$(MAKE) -C doc yorick.info
	mkdir -p build/share
	mkdir -p build/lib/yorick
	cp -a relocate build/share/yorick
	mv build/share/yorick/doc build
	mkdir -p build/share/yorick/packages/installed
	mv build/share/yorick/bin build/lib/yorick/
	mv build/share/yorick/lib build/lib/yorick/
	mkdir -p build/yorick-dev/share/yorick/
	mkdir -p build/yorick-dev/lib/yorick/lib/
	mv build/share/yorick/Make* build/yorick-dev/share/yorick/
	mv build/share/yorick/include build/yorick-dev/share/yorick/
	cd build/lib/yorick/lib; \
	   mv codger install.grp install.sh libdep.sh libyor.a main.o \
              $(CURDIR)/build/yorick-dev/lib/yorick/lib
	cp emacs/yorick-auto.el build/50yorick-auto.el
	touch build-stamp

build-mpy-stamp: build-stamp
ifeq ($(wildcard /usr/bin/mpicc.mpich), /usr/bin/mpicc.mpich)
	cp -a mpy mpy.mpich2
	(cd mpy.mpich2 ; \
	./configure --yorick=../relocate/bin/yorick --mpicc=mpicc.mpich ; \
	$(MAKE))
	mv mpy.mpich2/mpy mpy.mpich2/mpy.mpich2
	mv mpy.mpich2/libmpy.a mpy.mpich2/libmpy.mpich2.a
	mv mpy.mpich2/Makempy mpy.mpich2/Makempy.mpich2
	mv mpy.mpich2/mpymain.o mpy.mpich2/mpymain.mpich2.o
endif
ifeq ($(wildcard /usr/bin/mpicc.openmpi), /usr/bin/mpicc.openmpi)
	cp -a mpy mpy.openmpi
	(cd mpy.openmpi ; \
	./configure --yorick=../relocate/bin/yorick --mpicc=mpicc.openmpi ; \
	$(MAKE))
	mv mpy.openmpi/mpy mpy.openmpi/mpy.openmpi
	mv mpy.openmpi/libmpy.a mpy.openmpi/libmpy.openmpi.a
	mv mpy.openmpi/Makempy mpy.openmpi/Makempy.openmpi
	mv mpy.openmpi/mpymain.o mpy.openmpi/mpymain.openmpi.o
endif
	touch build-mpy-stamp

clean-mpy:
	rm -Rf build-mpy-stamp mpy.mpich2 mpy.openmpi

clean: configure-stamp clean-mpy
	dh_testdir
	rm -Rf build-stamp build-indep-stamp configure-stamp build relocate
	touch Make.cfg
	$(MAKE) -C doc/html clean
	$(MAKE) distclean
#	$(MAKE) Y_HOME=. ysite
	dh_clean

# Must not depend on anything. This is to be called by
# binary-arch/binary-indep
# in another 'make' thread.
binary-common:
	dh_testdir
	dh_testroot
	dh_prep 
	dh_installdirs
	dh_install
	dh_installchangelogs 
	dh_installdocs
	dh_installexamples
	dh_installmenu
	dh_installinfo
	dh_installdebconf
	dh_installman
	dh_link
	dh_strip -pyorick --dbgsym-migration='yorick-dbg (<< 2.2.04+dfsg1-7~)'
	dh_strip -pyorick-dev -pyorick-mpy-openmpi -pyorick-mpy-mpich2
	dh_usrlocal
	dh_compress 
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture independant packages using the common target.
#binary-indep: build-indep install-indep
binary-indep: build-indep
	$(MAKE) -f debian/rules DH_OPTIONS=-i binary-common

# Build architecture dependant packages using the common target.
binary-arch: build-arch check
	$(MAKE) -f debian/rules DH_OPTIONS=-a binary-common

###### Getting the original source:
Y_FULL_VERS:=$(shell dpkg-parsechangelog -l$(PATH_TO_DEBIAN)/changelog | grep Version | sed 's/Version: //' | sed 's/-[0-9].*//')
Y_VERS:=$(subst +dfsg,,$(Y_FULL_VERS))
GIT_TAG:=y_$(subst .,_,$(Y_VERS))
GIT_REPO=git://github.com/dhmunro/yorick.git

# Non-DFSG-free files (see copyright):
PRUNEFILES:=play/hacks/hack103.c i/idlsave.i opengl

# make orig.tar.bz2 from current source dir, pruning PRUNEFILES
orig-txz:
	rm -Rf $(PRUNEFILES)
	cd .. ; \
	  mv `basename $(CURDIR)` yorick-$(Y_FULL_VERS) ; \
	  tar cvf - yorick-$(Y_FULL_VERS) --exclude=.pc --exclude=.git \
	    --exclude=yorick-$(Y_FULL_VERS)/debian --exclude=.gitignore \
	    | xz > yorick_$(Y_FULL_VERS).orig.tar.xz


get-orig-source:
	-rm -Rf ../orig-sources
	mkdir ../orig-sources
	cd ../orig-sources ; \
	  git clone $(GIT_REPO) ; \
	  cd yorick ; \
	  git checkout $(GIT_TAG) ; \
	  rm -Rf $(PRUNEFILES)
	-rm -Rf ../yorick-$(Y_VERS)
	mv ../orig-sources/yorick ../yorick-$(Y_VERS)+dfsg
	rm -Rf ../orig-sources
	cd ..; tar cf - --exclude=.git --exclude=.gitignore \
	  yorick-$(Y_VERS)+dfsg | \
	  xz > yorick_$(Y_VERS)+dfsg.orig.tar.xz

gitclean: clean
	-quilt pop -a
	if test -e .pc ; then \
	   cd .pc ; \
	   rm .quilt_patches .quilt_series .version ; \
	   cd .. ; rmdir .pc ; \
	   fi

binary: binary-arch binary-indep
.PHONY: build-arch build-indep build clean binary-indep binary-arch binary \
	configure clean-mpy gitclean
